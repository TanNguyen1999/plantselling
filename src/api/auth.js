import { get, post } from './utils';

export async function adminLoginApi(data) {
  return post('/users/login', data);
}

export async function signUpApi(data) {
  return post('/users/register', data);
}

export async function forgotPasswordApi(data) {
  return post('/users/forgotPassword', data);
}

export async function confirmCodeApi(data) {
  return post('/users/resetPassword', data);
}

export async function fetchProductApi() {
  return get('/products');
}

export async function fetchUserApi(id) {
  return get(`/users/${id}`);
}

export async function fetchOrderHistoryApi(id) {
  return get(`/users/${id}/orderHistory`);
}
