import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  Text,
  Image,
  Picker,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux';
import _ from 'lodash';

import {
  pushScreen,
  showModal,
  showOverlay,
  showInAppNotification
} from '../../navigation/navigationConfig/serviceActions';
import {
  Container,
  SegmentedControlTab,
  ViewPagerFlatList,
  ViewMoreText,
  TouchableButton
} from '../../components';

import { iconsMap } from '../../utils/appIcons';
import FetchProductActions from '../../redux/ListProductRedux/actions';
import FetchProductTypes from '../../redux/ListProductRedux/reducer';
import { Colors } from '../../themes';
import { Button } from 'react-native-vector-icons/rnstandard';

class ShowProduct extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        drawBehind: false,
        animate: true,
        title: {
          text: 'Title'
        }
      }
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      language: 'vi',
      text: '',
      currentTabIndex: 0,
      count: 0
    };
  }

  componentDidMount() {
    this.props.onFetchProduct();
  }

  checkNavigation = type => {
    if (type === 'push') {
      pushScreen(this.props.componentId, 'app.testing', {}, {}, false);
    }
    if (type === 'showModal') {
      showModal('app.testing', {}, {}, true);
    }
    if (type === 'showOverlay') {
      showOverlay('app.testing', {}, true);
    }
  };

  focusNextField(nextField) {
    this[nextField].focus();
  }

  onChangeTab = index => {
    this.setState({ currentTabIndex: index });
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={{ margin: 10 }}>
        <Text style={{ fontStyle: 'italic', color: 'black' }}>
          Name of tree: {item.name}
        </Text>
      </View>
    );
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1
    });
  };

  render() {
    return (
      <FlatList
        data={this.props.product}
        renderItem={this.renderItem}
        keyExtractor={(item, index) => index}
      />
    );
  }
}
const mapStateToProps = state => ({
  product: state.listProduct.data
});

const mapDispatchToProps = dispatch => ({
  onFetchProduct: () => dispatch(FetchProductActions.fetchProduct())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowProduct);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    fontFamily: 'Roboto-Light'
  },
  image: {
    width: 200,
    height: 200,
    alignSelf: 'center'
  },
  MainContainer: {
    // Setting up View inside content in Vertically center.
    justifyContent: 'center',
    flex: 1,
    margin: 10
  },

  item: {
    padding: 10,
    fontSize: 18,
    textAlign: 'center'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  countContainer: {
    alignItems: 'center',
    padding: 10
  },
  countText: {
    color: 'green'
  },
  category: {
    width: 75,
    height: 75,
    backgroundColor: '#d0d3d8',
    margin: 20,
    borderRadius: 20
  }
});
