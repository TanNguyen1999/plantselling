import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  Share,
  Alert,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/rnstandard';
import Config from 'react-native-config';
import { Text, Touchable, RowDirect } from '../../components';
import { Colors } from '../../themes';
import {
  showModal,
  mergeOptions,
  showOverlay,
  pushScreen,
  showConfirmAlert,
  showFeedback
} from '../../navigation/navigationConfig/serviceActions';

import LoginActions from '../../redux/LoginRedux/actions';

class SideMenu extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: false,
        drawBehind: true
      }
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      avatar:
        'https://zmp3-photo-fbcrawler.zadn.vn/avatars/e/e/ee58fcc0ff45002b8d416bd7685809ce_1487040461.jpg'
    };
  }

  onAvatarChange = e => {
    this.setState({ avatar: e });
  };

  goToProfile = () => {
    mergeOptions(this.props.componentId, {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    showModal('app.userProfile');
  };

  onMoveToScreen = rowTitle => {
    pushScreen(
      this.props.sideMenuId,
      'app.forgotpassword',
      {},
      {
        sideMenu: {
          left: {
            visible: false
          }
        },
        topBarTitle: rowTitle
      }
    );
  };

  showConfirmAlert = () => {
    showConfirmAlert('Rating', 'You are totally into this app?', [
      {
        text: 'Cancel'
      },
      {
        text: 'Select'
      }
    ]);
  };

  showFeedbackPopup = () => {};

  logout = () => {
    showConfirmAlert('Logout', 'Are you sure to logout', [
      {
        text: 'Cancel'
      },
      {
        text: 'Submit',
        onPress: () => {
          this.props.logout();
        }
      }
    ]);
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'https://cdn2.hercampus.com/styles/hcxo_tile_standard/s3/hero-images/2018/11/10/274292f1-corgi.jpg?timestamp=1541912810'
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.safeAreaView}>
        <View style={styles.safeAreaView}>
          <ScrollView>
            <View style={styles.headerContainer}>
              <Touchable onPress={this.goToProfile}>
                <View style={styles.profileContainer}>
                  <View>
                    <Image
                      source={{
                        uri:
                          'https://cdn2.hercampus.com/styles/hcxo_tile_standard/s3/hero-images/2018/11/10/274292f1-corgi.jpg?timestamp=1541912810'
                      }}
                      style={styles.avatar}
                    />
                  </View>
                  <View style={styles.infoUser}>
                    <View>
                      <Text type="bold" color={Colors.black} sizeType="large">
                        Corgi Phạm
                      </Text>
                    </View>
                    <View style={styles.editContainer}>
                      <Text
                        type="light"
                        color={Colors.black}
                        sizeType="xMedium"
                        style={styles.txtEditProfile}
                      >
                        {'update profile'}
                      </Text>
                      <Icon name="ic-next" color={Colors.black} size={10} />
                    </View>
                  </View>
                </View>
              </Touchable>
            </View>
            <RowDirect rowTitle="Ưu đãi" onPress={this.onMoveToScreen} />
            <RowDirect
              rowTitle="Đã đặt trước"
              onPress={() =>
                Alert.alert(
                  'Update available',
                  'Keep your app up to date to enjoy the latest features',
                  [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel'
                    },
                    {
                      text: 'Install',
                      onPress: () => console.log('Install Pressed')
                    }
                  ]
                )
              }
            />
            <RowDirect rowTitle="Địa chỉ giao hàng" badge="New" />
            <RowDirect rowTitle="Chia sẻ" onPress={this.onShare} />
            <RowDirect rowTitle="Cài đặt" onPress={this.showFeedbackPopup} />
            <RowDirect
              icon="ic-logout"
              rowTitle={'Logout'}
              onPress={this.logout}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(LoginActions.logout())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu);

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1
  },
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingVertical: 30,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.divider
  },
  avatarContainer: {
    marginHorizontal: 5,
    borderWidth: 4,
    borderColor: Colors.primary
  },
  profileContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  editContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 5
  },
  txtEditProfile: {
    paddingRight: 5
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 100,
    margin: 10
  },
  infoUser: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  }
});
