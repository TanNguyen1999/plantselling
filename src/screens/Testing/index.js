import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/rnstandard';
import { iconsMap } from '../../utils/appIcons';
import LoginActions from '../../redux/LoginRedux/actions';
import AppActions from '../../redux/AppRedux/actions';
import MemberActions from '../../redux/MemberRedux/actions';
import { Colors, Metrics } from '../../themes';
import { Touchable } from '../../components/index';
import MemberRow from './MemberRow';
import { Text, EmptyView } from '../../components/index';
import { getMemberListSelector } from '../../redux/MemberRedux/selectors';
// import { star } from '../../navigation/navigationConfig/serviceActions';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import IonIcons from 'react-native-vector-icons/Ionicons';

class Testing extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        drawBehind: false,
        animate: true,
        title: {
          text: 'Tree for Life'
        },
        leftButtons: [
          {
            id: 'sideMenu',
            color: Colors.white
          }
        ]
      }
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      refreshing: false
    };
    this.viewabilityConfig = {
      waitForInteraction: true,
      itemVisiblePercentThreshold: 50
    };
    this.onEndReached = _.debounce(this.onEndReached, 1000);
    this.props.refreshPage(false);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.loading && !prevState.refreshing && !prevState.isReady) {
      return {
        isReady: true,
        refreshing: false
      };
    }

    if (!nextProps.loading && prevState.refreshing) {
      return {
        refreshing: false
      };
    }

    return null;
  }

  onEndReached = e => {
    const { data, loading, total } = this.props;
    if (loading) return;

    const len = (data && data.length) || 0;
    if (len < 10) {
      return;
    }

    if (len < total) {
      this.props.getNextPage(true);
    }
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    const maxIndexItem = _.maxBy(viewableItems, 'index');
    if (!maxIndexItem) {
      return;
    }
    const maxIndex = maxIndexItem.index;
    const currentPage = Math.floor(maxIndex / 10);
    if (currentPage !== this.props.reduxPage) {
      this.props.updatePage(currentPage);
    }
  };

  onRefresh = () => {
    this.setState({
      refreshing: true
    });
    this.props.refreshPage(false);
  };

  rollToTop = () => {
    if (this.flatListRef) {
      this.flatListRef.scrollToIndex({
        animated: true,
        index: 0
      });
    }
  };

  onPressItem = item => {};

  renderItem = ({ item, index }) => {
    return (
      <MemberRow rRow item={item} key={index} onPressItem={this.onPressItem} />
    );
  };

  renderLoading() {
    return (
      <View style={[styles.absoluteLoading]}>
        <ActivityIndicator color={Colors.primary} />
      </View>
    );
  }

  renderEmptyView = () => {
    const { loading } = this.props;
    return <EmptyView title={loading ? 'Loading' : 'Empty'} />;
  };

  renderSeperator = () => {
    return (
      <View
        style={{
          width: Metrics.screenWidth,
          height: 0.5,
          backgroundColor: Colors.divider
        }}
      />
    );
  };

  renderFooter = () => {
    if (this.state.refreshing) {
      return <View style={styles.loadMore} />;
    }

    const { total, data, loading } = this.props;
    if (loading) {
      return (
        <View style={styles.loadMore}>
          <ActivityIndicator color={Colors.primary} />
        </View>
      );
    }

    const len = data && data.length;
    const isEndOfList = len === total;

    if (len && isEndOfList) {
      return (
        <Touchable onPress={this.rollToTop}>
          <View style={styles.loadMore}>
            <Icon
              name="ic-next"
              size={14}
              color={Colors.black}
              style={{ transform: [{ rotate: '-90deg' }] }}
            />
          </View>
        </Touchable>
      );
    }

    return <View style={styles.loadMore} />;
  };

  render() {
    if (!this.state.isReady) {
      return this.renderLoading();
    }
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          ref={ref => {
            this.flatListRef = ref;
          }}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
          data={this.props.data}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
              tintColor={Colors.primary}
              colors={[Colors.primary]}
            />
          }
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}
          ListFooterComponent={this.renderFooter}
          ListEmptyComponent={this.renderEmptyView}
          ItemSeparatorComponent={this.renderSeperator}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.05}
          initialNumToRender={10}
          maxToRenderPerBatch={2}
          extraData={this.props.loading || this.props.total}
        />
      </SafeAreaView>
      // <Container>
      //   <SegmentedControlTab
      //     values={TAB_ITEMS}
      //     selectedIndex={currentTabIndex}
      //     onTabPress={this.onChangeTab}
      //     borderRadius={50}
      //     tabStyle={styles.tabStyle}
      //     activeTabStyle={styles.activeTabStyle}
      //     tabTextStyle={styles.tabTextStyle}
      //     activeTabTextStyle={styles.activeTabTextStyle}
      //     tabsContainerStyle={styles.tabContainerStyle}
      //   />
      //   <ViewPagerFlatList
      //     onChangeTab={this.onChangeTab}
      //     selectedIndex={currentTabIndex}
      //     scrollEnabled={true}
      //   >
      //     <ScrollView>
      //       <View style={{ flex: 1 }} tabIndex={currentTabIndex}>
      //         <TouchableButton
      //           title="Show Datepicker"
      //           buttonColor={Colors.error}
      //           onPress={this.onChangeLanguage}
      //           style={{ alignSelf: 'center', marginTop: 10 }}
      //         />

      //         <ViewMoreText
      //           numberOfLines={3}
      //           textStyle={{ paddingHorizontal: 10 }}
      //           textViewMoreStyle={{ paddingLeft: 10, paddingTop: 8 }}
      //           textViewLessStyle={{ paddingLeft: 10, paddingTop: 8 }}
      //           description="Lorem ipsum dolor sit amet, in quo dolorum ponderum, nam veri
      //           molestie constituto eu. Eum enim tantas sadipscing ne, ut omnes
      //           malorum nostrum cum. Errem populo qui ne, ea ipsum antiopam
      //           definitionem eos."
      //         />
      //       </View>
      //       <SearchBar onChangeText={text => console.log(text)} />
      //     </ScrollView>

      //     <View style={{ flex: 1 }} tabIndex={currentTabIndex} />
      //   </ViewPagerFlatList>
      // </Container>
    );
  }
}

const mapStateToProps = (state, props) => ({
  data: getMemberListSelector(state, props),
  total: state.member.memberTotal,
  reduxPage: state.member.memberPage,
  loading: state.member.loading
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(LoginActions.logout()),
  changeLanguage: language => dispatch(AppActions.changeLanguage(language)),
  startup: () => dispatch(AppActions.startup()),
  updatePage: page => dispatch(MemberActions.updatePage(page)),
  refreshPage: isNextPage => dispatch(MemberActions.getMemberList(isNextPage)),
  getNextPage: isNextPage => dispatch(MemberActions.getMemberList(isNextPage))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Testing);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  absoluteLoading: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loadMore: {
    height: 24,
    justifyContent: 'center',
    alignItems: 'center'
  }
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF'
  // },
  // welcome: {
  //   fontSize: 20,
  //   textAlign: 'center',
  //   margin: 10
  // },
  // instructions: {
  //   textAlign: 'center',
  //   color: '#333333',
  //   marginBottom: 5,
  //   fontFamily: 'Roboto-Light'
  // },
  // image: {
  //   width: 200,
  //   height: 200,
  //   alignSelf: 'center'
  // }
});
