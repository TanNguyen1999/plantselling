import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Touchable, Text, TouchableButton } from '../../components/index';
import {
  showModal,
  mergeOptions,
  showOverlay,
  pushScreen,
  showConfirmAlert,
  showFeedback
} from '../../navigation/navigationConfig/serviceActions';

export default class MemberRow extends React.PureComponent {
  onPressItem = () => {
    const { item, onPressItem } = this.props;
    onPressItem(item);
  };

  addToCart = () => {
    showConfirmAlert(
      'Add to Cart',
      'Would you like to add this product into cart?',
      [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          onPress: () => {
            //this.props.logout();
          }
        }
      ]
    );
  };

  render() {
    const { item } = this.props;
    const isPromotion = item.promotePrice === 0;
    return (
      <Touchable onPress={this.addToCart}>
        <View>
          {isPromotion ? (
            <View
              style={{
                flexDirection: 'row',
                paddingVertical: 20,
                paddingHorizontal: 15,
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Image source={{ uri: item.image }} style={styles.product} />

              <View style={{ flexDirection: 'column' }}>
                <View style={{ flex: 1, width: 70 }}>
                  <Text>{item.name}</Text>
                </View>
                <View style={{ flex: 1 }} />
              </View>

              <View style={{ flexDirection: 'column' }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontWeight: 'bold', marginLeft: 50 }}>
                    {item.price}
                  </Text>
                </View>
                <View style={{ flex: 1 }} />
              </View>
            </View>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                paddingVertical: 20,
                paddingHorizontal: 15,
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Image source={{ uri: item.image }} style={styles.product} />
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flex: 1, width: 60 }}>
                  <Text>{item.name}</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text>Old Price: {item.price}</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'column' }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontWeight: 'bold', marginLeft: 30 }}>
                    {item.promotePrice}
                  </Text>
                </View>
                <View style={{ flex: 1 }} />
              </View>
            </View>
          )}
        </View>
      </Touchable>
    );
  }
}
const styles = StyleSheet.create({
  product: {
    width: 100,
    height: 100,
    borderRadius: 20,
    marginRight: 10
  }
});
