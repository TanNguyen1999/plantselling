import React from 'react';
import { StyleSheet, Image, Keyboard, View } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  KeyboardAwareScrollView,
  RoundInput,
  TouchableButton
} from '../../components';
import { Images, Colors, Metrics } from '../../themes';
import { pushScreen } from '../../navigation/navigationConfig/serviceActions';
import LoginActions from '../../redux/LoginRedux/actions';

class ConfirmCode extends React.Component {
  static options(passProps) {
    return {
      topBar: {}
    };
  }
  constructor(props) {
    super(props);
    this.state = {};
  }

  onConfirm = () => {
    if (!this.email.getText()) {
      this.email.focus();
      return null;
    }
    if (!this.newPassword.getText()) {
      this.newPassword.focus();
      return null;
    }
    if (!this.confirmPassword.getText()) {
      this.confirmPassword.focus();
      return null;
    }
    if (!this.codeConfirm.getText()) {
      this.codeConfirm.focus();
      return null;
    }
    const data = {
      email: this.email.getText(),
      codeConfirm: this.codeConfirm.getText(),
      newPassword: this.newPassword.getText(),
      confirmPassword: this.confirmPassword.getText()
    };

    this.props.confirmCode(data);
  };
  onMoveToConfirmCode = () => {};
  render() {
    return (
      <Container>
        <KeyboardAwareScrollView>
          <Image source={Images.logo} resizeMode="cover" style={styles.logo} />
          <View style={styles.inputContainer}>
            <RoundInput
              ref={ref => {
                this.email = ref;
              }}
              isRequired
              validateType="email"
              errorMessage={'Email is invalid'}
              placeholder={'Email'}
              defaultValue={this.props.email}
              onSubmitEditing={() => {
                this.focusNextField('newPassword');
              }}
            />

            <RoundInput
              ref={ref => {
                this.newPassword = ref;
              }}
              isRequired
              validateType="password"
              errorMessage={'Password is invalid'}
              placeholder={'New Password'}
              secureTextEntry
              onSubmitEditing={() => {
                this.focusNextField('confirmPassword');
              }}
            />

            <RoundInput
              ref={ref => {
                this.confirmPassword = ref;
              }}
              isRequired
              validateType="password"
              errorMessage={'Password is invalid'}
              placeholder={'Confirm Password'}
              secureTextEntry
              onSubmitEditing={() => {
                this.focusNextField('codeConfirm');
              }}
            />

            <RoundInput
              ref={ref => {
                this.codeConfirm = ref;
              }}
              isRequired
              validateType="empty"
              errorMessage={'Confirm code is invalid'}
              placeholder={'Confirm Code'}
            />
          </View>
          <TouchableButton
            title={'Submit'}
            buttonColor={Colors.primary}
            style={styles.buttonConfirm}
            onPress={this.onConfirm}
          />
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  confirmCode: data => dispatch(LoginActions.confirmCode(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmCode);

const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 150,
    alignSelf: 'center',
    marginVertical: 20
  },
  inputContainer: {
    marginHorizontal: 20
  },
  buttonConfirm: {
    width: Metrics.screenWidth - 50,
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 50
  }
});
