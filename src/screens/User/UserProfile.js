import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  Text,
  Image,
  Picker,
  TouchableOpacity,
  FlatList
} from 'react-native';

import { connect } from 'react-redux';
import _ from 'lodash';

import {
  pushScreen,
  showModal,
  showOverlay,
  showInAppNotification
} from '../../navigation/navigationConfig/serviceActions';
import {
  Container,
  SegmentedControlTab,
  ViewPagerFlatList,
  ViewMoreText,
  TouchableButton
} from '../../components';

import { iconsMap } from '../../utils/appIcons';
import FetchUserActions from '../../redux/UserProfileRedux/actions';
import { Colors } from '../../themes';
import { Button } from 'react-native-vector-icons/rnstandard';

class UserProfile extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        drawBehind: false,
        animate: true,
        title: {
          text: 'User Profile'
        }
      }
    };
  }

  componentDidMount() {
    //this.props.onFetchUser();
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={{ margin: 10 }}>
        <Text style={{ fontStyle: 'italic', color: 'black' }}>
          Name of user: {item.name}
        </Text>
      </View>
    );
  };

  render() {
    console.log('====================================');
    console.log(this.props.orderData.map(item => item.id));
    console.log('====================================');
    return (
      // <FlatList
      //   data={this.props.userData}
      //   renderItem={this.renderItem}
      //   keyExtractor={(item, index) => index}
      // />
      <View style={styles.MainContainer}>
        <View style={styles.profileCover}>
          <Image
            source={{
              uri:
                'https://cdn2.hercampus.com/styles/hcxo_tile_standard/s3/hero-images/2018/11/10/274292f1-corgi.jpg?timestamp=1541912810'
            }}
            style={styles.imageStyle}
          />
        </View>
        <View style={styles.userProfile}>
          <View style={styles.instructions}>
            <Text>{this.props.orderData.map(item => item.id)}</Text>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  userData: state.userProfile.data,
  orderData: state.orderHistory.data
});

const mapDispatchToProps = dispatch => ({
  //onFetchUser: () => dispatch(FetchUserActions.fetchUser())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  profileCover: {
    flex: 1,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center'
  },
  userProfile: {
    flex: 3
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    fontFamily: 'Roboto-Light'
  },
  image: {
    width: 200,
    height: 200,
    alignSelf: 'center'
  },
  MainContainer: {
    // Setting up View inside content in Vertically center.
    justifyContent: 'center',
    flex: 1,
    margin: 10
  },

  item: {
    padding: 10,
    fontSize: 18,
    textAlign: 'center'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  countContainer: {
    alignItems: 'center',
    padding: 10
  },
  countText: {
    color: 'green'
  },
  imageStyle: {
    width: 180,
    height: 180,
    backgroundColor: '#d0d3d8',
    marginTop: 100,
    borderRadius: 90,
    borderWidth: 0.5,
    borderColor: '#a6d7dd'
  }
});
