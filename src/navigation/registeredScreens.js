import React from 'react';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

// import your screens here
import Testing from '../screens/Testing';
import Intro from '../screens/Intro';
import SideMenu from '../screens/SideMenu';
import Login from '../screens/Authentication/Login';
import ForgotPassword from '../screens/Authentication/ForgotPassword';
import ConfirmCode from '../screens/Authentication/ConfirmCode';
import SignUp from '../screens/Authentication/Signup';
import ShowProduct from '../screens/Product/ShowProduct';
import UserProfile from '../screens/User/UserProfile';

// import your components here
import {
  ConfirmAlert,
  Feedback,
  DatePicker,
  InAppNotification
} from '../components/Overlay';

export function registerScreens(store, persistor) {
  const PersistProvider = props => {
    const { children } = props;
    return (
      <Provider {...props}>
        <PersistGate loading={null} persistor={persistor}>
          {children}
        </PersistGate>
      </Provider>
    );
  };

  // screens
  Navigation.registerComponentWithRedux(
    'app.testing',
    () => Testing,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.intro',
    () => Intro,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.sidemenu',
    () => SideMenu,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.login',
    () => Login,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.forgotpassword',
    () => ForgotPassword,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.confirmcode',
    () => ConfirmCode,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.signup',
    () => SignUp,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.showProduct',
    () => ShowProduct,
    PersistProvider,
    store
  );

  Navigation.registerComponentWithRedux(
    'app.userProfile',
    () => UserProfile,
    PersistProvider,
    store
  );

  // components
  Navigation.registerComponent(
    'overlay.confirmAlert',
    () => ConfirmAlert,
    () => ConfirmAlert,
    PersistProvider,
    store
  );

  Navigation.registerComponent(
    'overlay.feedback',
    () => Feedback,
    () => Feedback,
    PersistProvider,
    store
  );

  Navigation.registerComponent(
    'overlay.datePicker',
    () => DatePicker,
    () => DatePicker,
    Provider,
    store
  );

  Navigation.registerComponent(
    'overlay.inAppNotification',
    () => InAppNotification,
    () => InAppNotification,
    PersistProvider,
    store
  );
}
