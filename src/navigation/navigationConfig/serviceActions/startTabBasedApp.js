import { Navigation } from 'react-native-navigation';
import { component, stack, bottomTabs, sideMenu } from '../layoutTypes';
import { iconsMap } from '../../../utils/appIcons';

export const startTabBasedApp = (hasSideMenu = false) => {
  if (hasSideMenu) {
    Navigation.setRoot({
      root: sideMenu(
        component('app.sidemenu'),
        [
          stack(
            component(
              'app.testing',
              {},
              {
                bottomTab: {
                  text: 'Home',
                  icon: iconsMap['ic-home']
                }
              }
            )
          ),
          stack(
            component(
              'app.testing',
              {},
              {
                bottomTab: {
                  text: 'Profile',
                  icon: iconsMap['ic-user']
                }
              }
            )
          )
        ],
        {},
        true
      )
    });
  } else {
    Navigation.setRoot({
      root: bottomTabs([
        stack(
          component(
            'app.testing',
            {},
            {
              bottomTab: {
                text: 'Home',
                icon: iconsMap['ic-home']
              }
            }
          )
        ),
        stack(
          component(
            'app.testing',
            {},
            {
              bottomTab: {
                text: 'Profile',
                icon: iconsMap['ic-user-1']
              }
            }
          )
        )
      ])
    });
  }
};
