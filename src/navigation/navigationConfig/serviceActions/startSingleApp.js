import { Navigation } from 'react-native-navigation';
import { component, stack, sideMenu } from '../layoutTypes';
import { iconsMap } from '../../../utils/appIcons';

export const startSingleApp = (screen, hasSideMenu = true) => {
  if (hasSideMenu) {
    Navigation.setRoot({
      root: sideMenu(
        component('app.sidemenu', { sideMenuId: screen }),
        component(screen, {}, {}, screen),
        {}
      )
    });
  } else {
    Navigation.setRoot({
      root: stack(component(screen))
    });
  }
};
