import login from './LoginRedux/reducer';
import app from './AppRedux/reducer';
import member from './MemberRedux/reducer';
import listProduct from './ListProductRedux/reducer';
import userProfile from './UserProfileRedux/reducer';
import orderHistory from './OrderHistoryRedux/reducer';

export default {
  login,
  app,
  member,
  listProduct,
  userProfile,
  orderHistory
};
