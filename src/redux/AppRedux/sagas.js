import { takeLatest, select } from 'redux-saga/effects';
import { AppTypes } from '../AppRedux/actions';
import {
  startSingleApp,
  showBottomTab
} from '../../navigation/navigationConfig/serviceActions';

export function* startup() {
  try {
    const { token } = yield select(state => state.login);
    const { language } = yield select(state => state.app);
    !token
      ? startSingleApp('app.intro', false)
      : showBottomTab('app.testing', 'app.testing', 'app.sidemenu');
  } catch (error) {}
}

const appSagas = () => [takeLatest(AppTypes.STARTUP, startup)];

export default appSagas();
