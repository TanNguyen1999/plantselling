import { makeActionCreator, makeConstantCreator } from '../../utils/reduxUtils';

export const fetchOrderHistoryTypes = makeConstantCreator(
  'FETCH_ORDER_HISTORY',
  'FETCH_ORDER_HISTORY_SUCCESS',
  'FETCH_ORDER_HISTORY_FAILURE'
);

const fetchOrderHistory = id =>
  makeActionCreator(fetchOrderHistoryTypes.FETCH_ORDER_HISTORY, { id });
const fetchOrderHistorySuccess = response =>
  makeActionCreator(fetchOrderHistoryTypes.FETCH_ORDER_HISTORY_SUCCESS, {
    response
  });
const fetchOrderHistoryFailure = error =>
  makeActionCreator(fetchOrderHistoryTypes.FETCH_ORDER_HISTORY_FAILURE, {
    error
  });

export default {
  fetchOrderHistory,
  fetchOrderHistorySuccess,
  fetchOrderHistoryFailure
};
