import Immutable from 'seamless-immutable';
import { fetchOrderHistoryTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const INITIAL_STATE = Immutable({
  data: [],
  error: null,
  fetchOrderHistoryLoading: false
});

const fetchOrderHistory = (state, { id }) =>
  state.merge({
    fetchOrderHistoryLoading: true
  });

const fetchOrderHistorySuccess = (state, { response }) =>
  state.merge({
    data: response,
    fetchOrderHistoryLoading: false
  });

const fetchOrderHistoryFailure = (state, { error }) =>
  state.merge({
    error: error,
    fetchOrderHistoryLoading: false
  });

const ACTION_HANDLERS = {
  [fetchOrderHistoryTypes.FETCH_ORDER_HISTORY]: fetchOrderHistory,
  [fetchOrderHistoryTypes.FETCH_ORDER_HISTORY_SUCCESS]: fetchOrderHistorySuccess,
  [fetchOrderHistoryTypes.FETCH_ORDER_HISTORY_FAILURE]: fetchOrderHistoryFailure
};

export default makeReducerCreator(INITIAL_STATE, ACTION_HANDLERS);
