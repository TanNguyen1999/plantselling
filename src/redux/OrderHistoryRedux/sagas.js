import { put, call, takeLatest } from 'redux-saga/effects';
import { fetchOrderHistoryApi } from '../../api/auth';
import FetchOrderHistoryActions, { fetchOrderHistoryTypes } from './actions';

export function* fetchOrderHistorySaga({ id }) {
  try {
    const data = yield call(fetchOrderHistoryApi, id);
    yield put(FetchOrderHistoryActions.fetchOrderHistorySuccess(data));
  } catch (error) {
    yield put(FetchOrderHistoryActions.fetchOrderHistoryFailure(error));
  }
}

const fetchOrderHistorySagas = () => [
  takeLatest(fetchOrderHistoryTypes.FETCH_ORDER_HISTORY, fetchOrderHistorySaga)
];

export default fetchOrderHistorySagas();
