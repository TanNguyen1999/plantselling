import { makeActionCreator, makeConstantCreator } from '../../utils/reduxUtils';

export const fetchUserTypes = makeConstantCreator(
  'FETCH_USER',
  'FETCH_USER_SUCCESS',
  'FETCH_USER_FAILURE'
);

const fetchUser = id => makeActionCreator(fetchUserTypes.FETCH_USER, { id });
const fetchUserSuccess = response =>
  makeActionCreator(fetchUserTypes.FETCH_USER_SUCCESS, { response });
const fetchUserFailure = error =>
  makeActionCreator(fetchUserTypes.FETCH_USER_FAILURE, { error });

export default {
  fetchUser,
  fetchUserSuccess,
  fetchUserFailure
};
