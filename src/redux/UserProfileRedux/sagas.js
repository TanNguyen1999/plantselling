import { put, call, takeLatest } from 'redux-saga/effects';
import { fetchUserApi } from '../../api/auth';
import FetchUserActions, { fetchUserTypes } from './actions';

export function* fetchUserSaga({ id }) {
  try {
    const data = yield call(fetchUserApi, id);
    yield put(FetchUserActions.fetchUserSuccess(data));
  } catch (error) {
    yield put(FetchUserActions.fetchUserFailure(error));
  }
}

const fetchUserSagas = () => [
  takeLatest(fetchUserTypes.FETCH_USER, fetchUserSaga)
];

export default fetchUserSagas();
