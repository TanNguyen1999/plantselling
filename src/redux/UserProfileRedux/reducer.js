import Immutable from 'seamless-immutable';
import { fetchUserTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const INITIAL_STATE = Immutable({
  data: {},
  error: null,
  fetchUserLoading: false
});

const fetchUser = (state, { id }) =>
  state.merge({
    fetchUserLoading: true
  });

const fetchUserSuccess = (state, { response }) =>
  state.merge({
    data: response,
    fetchUserLoading: false
  });

const fetchUserFailure = (state, { error }) =>
  state.merge({
    error: error,
    fetchUserLoading: false
  });

const ACTION_HANDLERS = {
  [fetchUserTypes.FETCH_USER]: fetchUser,
  [fetchUserTypes.FETCH_USER_SUCCESS]: fetchUserSuccess,
  [fetchUserTypes.FETCH_USER_FAILURE]: fetchUserFailure
};

export default makeReducerCreator(INITIAL_STATE, ACTION_HANDLERS);
