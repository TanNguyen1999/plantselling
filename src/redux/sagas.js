import { all } from 'redux-saga/effects';
import loginSagas from './LoginRedux/sagas';
import appSagas from './AppRedux/sagas';
import memberSagas from './MemberRedux/sagas';
import fetchProductSaga from './ListProductRedux/sagas';
import fetchUserSagas from './UserProfileRedux/sagas';
import fetchOrderHistorySagas from './OrderHistoryRedux/sagas';

export default function* root() {
  yield all([
    ...loginSagas,
    ...appSagas,
    ...memberSagas,
    ...fetchProductSaga,
    ...fetchUserSagas,
    ...fetchOrderHistorySagas
  ]);
}
