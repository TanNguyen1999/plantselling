import { put, call, takeLatest } from 'redux-saga/effects';
import {
  adminLoginApi,
  signUpApi,
  forgotPasswordApi,
  confirmCodeApi
} from '../../api/auth';

import LoginActions, { LoginTypes } from '../LoginRedux/actions';

import AppActions from '../AppRedux/actions';
import {
  showOverlay,
  showInAppNotification,
  pushScreen,
  showBottomTab
} from '../../navigation/navigationConfig/serviceActions';
import UserActions from '../UserProfileRedux/actions';
import OrderHistoryActions from '../OrderHistoryRedux/actions';
export function* logoutSaga() {
  try {
    yield put(AppActions.startup());
    global.token = null;
  } catch (error) {}
}

export function* adminLoginSaga({ data }) {
  try {
    const response = yield call(adminLoginApi, data);
    console.log(response.id);
    yield put(LoginActions.loginSuccess(response));
    global.token = response.token;
    yield put(AppActions.startup());
    yield put(UserActions.fetchUser(response.id));
    yield put(OrderHistoryActions.fetchOrderHistory(response.id));
    showInAppNotification('Sign In', 'Chào mừng bạn đến với Tree of Life');
  } catch (error) {
    yield put(LoginActions.loginFailure(error));
    if (error.code === 403) {
      return showInAppNotification('Sign In', error.message, 'error');
    }

    return showInAppNotification('Sign In', 'Check your connection', 'error');
  }
}

export function* forgotPasswordSaga({ data }) {
  try {
    console.log(data);
    const response = yield call(forgotPasswordApi, data);
    console.log(response);

    yield put(LoginActions.forgotPasswordSuccess(response));
    showInAppNotification('Forgot Password', 'Đã gửi email xác nhận');
    // pushScreen(
    //   this.props.componentId,
    //   'app.confirmcode',
    //   {},
    //   { topBarTitle: 'Confirm Code' }
    // );
  } catch (error) {
    yield put(LoginActions.forgotPasswordFailure(error));
  }
}

export function* signUpSaga({ data }) {
  try {
    console.log(data);
    const response = yield call(signUpApi, data);
    console.log(response);

    yield put(LoginActions.signUpSuccess(response));
    global.token = response.token;
    yield put(AppActions.startup());
    yield put(UserActions.fetchUser(response.id));
    showInAppNotification('Sign Up', 'Đăng kí thành công');
  } catch (error) {
    yield put(LoginActions.signUpFailure(error));
    if (error.code === 403) {
      return showInAppNotification('Sign Up', error.message, 'error');
    }
    return showInAppNotification('Sign Up', 'Check your connection', 'error');
  }
}

export function* confirmCodeSaga({ data }) {
  try {
    console.log(data);
    const response = yield call(confirmCodeApi, data);
    console.log(response);

    yield put(LoginActions.confirmCodeSuccess(response));
    global.token = response.token;

    yield put(AppActions.startup());
    showInAppNotification('Sign In', 'Chào mừng bạn đến với Tree of Life');
  } catch (error) {
    yield put(LoginActions.confirmCodeFailure(error));
    if (error.code === 403) {
      return showInAppNotification('Sign In', error.message, 'error');
    }

    return showInAppNotification('Sign In', 'Check your connection', 'error');
  }
}

const loginSagas = () => [
  takeLatest(LoginTypes.LOGIN, adminLoginSaga),
  takeLatest(LoginTypes.LOGOUT, logoutSaga),
  takeLatest(LoginTypes.FORGOT_PASSWORD, forgotPasswordSaga),
  takeLatest(LoginTypes.SIGNUP, signUpSaga),
  takeLatest(LoginTypes.CONFIRM_CODE, confirmCodeSaga)
];

export default loginSagas();
