import Immutable from 'seamless-immutable';
import { LoginTypes } from './actions';

import { makeReducerCreator } from '../../utils/reduxUtils';

export const INITIAL_STATE = Immutable({
  data: {},
  forgotPassword: false,
  error: null,
  token: null,
  loginLoading: false,
  forgotPasswordLoading: false,
  signUpLoading: false,
  confirmCodeLoading: false
});

const login = (state, { data }) =>
  state.merge({
    loginLoading: true
  });

const loginSuccess = (state, { response }) =>
  state.merge({
    data: response,
    token: response.token,
    loginLoading: false
  });

const loginFailure = (state, { error }) =>
  state.merge({
    error: error,
    loginLoading: false
  });

const logout = state => INITIAL_STATE;

const forgotPassword = (state, { data }) =>
  state.merge({
    forgotPasswordLoading: true
  });

const forgotPasswordSuccess = (state, { response }) =>
  state.merge({
    forgotPassword: response,
    forgotPasswordLoading: false
  });

const forgotPasswordFailure = (state, { error }) =>
  state.merge({
    error: error,
    forgotPasswordLoading: false
  });

const signUp = (state, { data }) =>
  state.merge({
    signUpLoading: true
  });

const signUpSuccess = (state, { response }) =>
  state.merge({
    data: response,
    token: response.token,
    signUpLoading: false
  });

const signUpFailure = (state, { error }) =>
  state.merge({
    error: error,
    signUpLoading: false
  });

const confirmCode = (state, { data }) =>
  state.merge({
    confirmCodeLoading: true
  });

const confirmCodeSuccess = (state, { response }) =>
  state.merge({
    data: response,
    token: response.token,
    confirmCodeLoading: false
  });

const confirmCodeFailure = (state, { error }) =>
  state.merge({
    error: error,
    confirmCodeLoading: false
  });

const ACTION_HANDLERS = {
  [LoginTypes.LOGIN]: login,
  [LoginTypes.LOGIN_SUCCESS]: loginSuccess,
  [LoginTypes.LOGIN_FAILURE]: loginFailure,
  [LoginTypes.LOGOUT]: logout,

  [LoginTypes.FORGOT_PASSWORD]: forgotPassword,
  [LoginTypes.FORGOT_PASSWORD_SUCCESS]: forgotPasswordSuccess,
  [LoginTypes.FORGOT_PASSWORD_FAILURE]: forgotPasswordFailure,

  [LoginTypes.SIGNUP]: signUp,
  [LoginTypes.SIGNUP_SUCCESS]: signUpSuccess,
  [LoginTypes.SIGNUP_FAILURE]: signUpFailure,

  [LoginTypes.CONFIRM_CODE]: confirmCode,
  [LoginTypes.CONFIRM_CODE_SUCCESS]: confirmCodeSuccess,
  [LoginTypes.CONFIRM_CODE_FAILURE]: confirmCodeFailure
};

export default makeReducerCreator(INITIAL_STATE, ACTION_HANDLERS);
